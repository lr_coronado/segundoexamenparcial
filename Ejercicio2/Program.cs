﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Empleado
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal Salario { get; set; }

        public Empleado() { }

        public Empleado (string nombre, decimal salario) 
        {
            this.Nombre = nombre;
            this.Salario = salario;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Luis Rafael Coronado | 2020-10295
            Empleado[] empleados = { new Empleado("Luis", 45000), new Empleado("Ramon", 15000), new Empleado("Miguel", 85000), new Empleado("Jose", 29000), new Empleado("Maria", 70000) };

            decimal MayorSueldo = 0;

            foreach (Empleado x in empleados)
            {
                if (x.Salario > MayorSueldo)
                {
                    MayorSueldo = x.Salario;
                }
            }

            Empleado empMayor = new Empleado();
            foreach (Empleado x in empleados) 
            {
                if (x.Salario == MayorSueldo) 
                {
                    empMayor = x;
                }
            }

            Console.WriteLine("El Empleado con el mayor salario es: {0}", empMayor.Nombre);
            Console.WriteLine("Tiene un Salario de: {0:C}", empMayor.Salario);
            Console.WriteLine("");
            Console.ReadLine();
        }
    }
}
