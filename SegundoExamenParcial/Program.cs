﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegundoExamenParcial
{
    class Maquina 
    {
        public int Id { get; set; }
        public List<Producto> Productos = new List<Producto>();
        public List<Producto> ProductosVendidos = new List<Producto>();
        public void AddProductoAlaMaquina (Producto producto)
        {
            this.Productos.Add(producto);
        }
        public bool VenderProdcuto(int id, decimal dinero) 
        {
            bool venta = true;
            foreach (Producto x in Productos)
            {
                if (x.Id == id)
                    if (x.Existencia > 0) 
                    {
                        if (dinero > 25)
                        {
                            Console.WriteLine("La Maquina no acepta billetes.");
                            venta = false;
                        }
                        else 
                        {
                            while (x.Precio > dinero)
                            {
                                Console.Write("Ingresa la Siguiente Moneda: ");
                                dinero += decimal.Parse(Console.ReadLine());
                            }

                            x.Existencia--;
                            decimal resto = x.Precio - dinero;
                            ProductosVendidos.Add(x);
                            Console.WriteLine("Devuelta: {0:C}", resto);

                            venta = true;
                        }
                    }
                    else
                        venta = false;
            }
            return venta;
        }
    }
    class Producto 
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public int Existencia { get; set; }

        public Producto(int id, string nombre, decimal precio, int existencia) 
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Precio = precio;
            this.Existencia = existencia;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Luis Rafael Coronado | 2020-10295

            Maquina maquina = new Maquina();
            maquina.AddProductoAlaMaquina(new Producto(1, "Empanada", 25, 2));
            maquina.AddProductoAlaMaquina(new Producto(2, "Chicle", 10, 3));
            maquina.AddProductoAlaMaquina(new Producto(3, "Dulce", 5, 1));
            maquina.AddProductoAlaMaquina(new Producto(4, "Biscocho", 35, 4));


            for ( ; ; ) 
            {
                Console.Write("Opciones: 1- Comprar  | 2- Informe | 3- Cerrar : ");
                int a = 0;
                int option = int.Parse(Console.ReadLine());
                Console.WriteLine("");
                if (option == 1)
                {
                    Console.WriteLine("Elige el Id de un producto: ");
                    Console.WriteLine("Prodcutos Disponibles: ");
                    foreach (Producto x in maquina.Productos) 
                    {
                        if (x.Existencia > 0) 
                        {
                            Console.WriteLine($"{x.Id}-, N:{x.Nombre}, RD{x.Precio:C}, Ext:{x.Existencia}");
                        }
                    }
                    Console.WriteLine("");
                    int id = int.Parse(Console.ReadLine());

                    Console.WriteLine("Introduce una moneda: (5,10,25)");
                    decimal moneda = decimal.Parse(Console.ReadLine());

                    bool venta = maquina.VenderProdcuto(id, moneda);

                    if(venta == true)
                        Console.WriteLine("Producto Vendido Exitosamente.");
                    else
                        Console.WriteLine("No hubo Venta.");
                    
                    Console.WriteLine("");

                }
                else if (option == 2)
                {
                    foreach (Producto x in maquina.ProductosVendidos) 
                    {
                        Console.WriteLine($"{x.Id}, {x.Nombre}, {x.Precio:C}");
                    }
                    Console.WriteLine("");
                }
                else
                {
                    return;
                }
            }
        }
    }
}
